use amethyst::{
    core::Time,
    derive::SystemDesc,
    ecs::{DispatcherBuilder, Read, System, SystemData, Write},
    Error,
};

#[derive(Default)]
pub struct GameTime {
    subsecond: f32,
    time: i64,
}

impl GameTime {
    pub fn update(&mut self, time: f32) {
        self.subsecond += time;
        if self.subsecond > 1.0 {
            self.time += self.subsecond.trunc() as i64;
            self.subsecond = self.subsecond.fract();
        }
    }

    pub fn format(&self) -> String {
        use chrono::prelude::*;
        let naive = NaiveDateTime::from_timestamp(self.time, 0);
        let datetime: DateTime<Utc> = DateTime::from_utc(naive, Utc);
        datetime.format("%Y-%B-%d %H:%M:%S").to_string()
    }
}

#[derive(SystemDesc)]
pub struct TimeSystem;

impl<'s> System<'s> for TimeSystem {
    type SystemData = (Read<'s, Time>, Write<'s, GameTime>);

    fn run(&mut self, (time, mut gt): Self::SystemData) {
        gt.update(time.delta_seconds());
    }
}

pub(super) fn register_systems<'a, 'b>(
    builder: &mut DispatcherBuilder<'a, 'b>,
) -> Result<(), Error> {
    builder.add(TimeSystem, "time_system", &[]);
    Ok(())
}
