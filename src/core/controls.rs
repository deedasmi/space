#![allow(unused_variables, dead_code)]
use super::*;
use amethyst::{
    controls::WindowFocus,
    core::{Time, Transform},
    derive::SystemDesc,
    ecs::{Join, Read, System, SystemData, Write, WriteStorage},
    input::{InputHandler, StringBindings},
    renderer::{camera::Projection::Orthographic, Camera},
};

const MOVE_INCREMENT: f32 = 10.0;
const ZOOM_INCREMENT: f32 = 100.0;
const SPEED_INCREMENTS: [f32; 12] = [
    0.0,
    1.0,
    2.0,
    5.0,
    60.0,
    300.0,
    1_800.0,
    3_600.0,
    28_800.0,
    86_400.0,
    604_800.0,
    2_592_000.0,
];
const INPUT_DELAY: f32 = 0.15;

#[derive(Default, SystemDesc)]
pub struct CameraMoveSystem {
    zoom_delay: f32,
    move_delay: f32,
    zoom_multiplier: f32,
}

impl<'s> System<'s> for CameraMoveSystem {
    type SystemData = (
        Read<'s, InputHandler<StringBindings>>,
        Read<'s, WindowFocus>,
        WriteStorage<'s, Camera>,
        WriteStorage<'s, Transform>,
        Read<'s, Time>,
    );

    fn run(&mut self, (input, focus, mut camera, mut transform, time): Self::SystemData) {
        if focus.is_focused {
            for (camera, transform) in (&mut camera, &mut transform).join() {
                //TODO prevent overzooming (inverts camera)
                let proj = camera.projection_mut();
                if let Some(zoom) = input.axis_value("Zoom") {
                    if zoom > 0.0 {
                        match proj {
                            Orthographic(x) => {
                                x.set_bottom_and_top(
                                    x.bottom() - ZOOM_INCREMENT,
                                    x.top() + ZOOM_INCREMENT,
                                );
                                x.set_left_and_right(
                                    x.left() - ZOOM_INCREMENT,
                                    x.right() + ZOOM_INCREMENT,
                                );
                            }
                            _ => unreachable!("Bad projection!"),
                        }
                    } else if zoom.is_sign_negative() {
                        match proj {
                            Orthographic(x) => {
                                x.set_bottom_and_top(
                                    x.bottom() + ZOOM_INCREMENT,
                                    x.top() - ZOOM_INCREMENT,
                                );
                                x.set_left_and_right(
                                    x.left() + ZOOM_INCREMENT,
                                    x.right() - ZOOM_INCREMENT,
                                );
                            }
                            _ => unreachable!("Bad projection!"),
                        }
                    }
                }
                if 0.0 < self.move_delay && self.move_delay < INPUT_DELAY {
                    self.move_delay += time.delta_real_seconds();
                } else {
                    if let Some(vert) = input.axis_value("Vertical") {
                        if vert > 0.0 {
                            transform.move_up(MOVE_INCREMENT);
                        } else if vert.is_sign_negative() {
                            transform.move_down(MOVE_INCREMENT);
                        }
                        self.move_delay = 0.00001;
                    }
                    if let Some(hor) = input.axis_value("Horizontal") {
                        if hor > 0.0 {
                            transform.move_right(MOVE_INCREMENT);
                        } else if hor.is_sign_negative() {
                            transform.move_left(MOVE_INCREMENT);
                        }
                        self.move_delay = 0.00001;
                    }
                }
            }
        }
    }
}

#[derive(Default, SystemDesc)]
pub struct SetSpeedSystem {
    index: usize,
    input_delay: f32,
}

impl<'s> System<'s> for SetSpeedSystem {
    type SystemData = (
        Write<'s, Time>,
        Read<'s, InputHandler<StringBindings>>,
        Read<'s, WindowFocus>,
    );

    fn run(&mut self, (mut gs, input, focus): Self::SystemData) {
        if 0.0 < self.input_delay && self.input_delay < INPUT_DELAY {
            self.input_delay += gs.delta_real_seconds();
        } else if focus.is_focused {
            if input.action_is_down("GameSpeedUp").unwrap_or(false)
                && self.index < SPEED_INCREMENTS.len() - 1
            {
                self.index += 1;
            }
            if input.action_is_down("GameSpeedDown").unwrap_or(false) && self.index > 0 {
                self.index -= 1;
            }
            gs.set_time_scale(SPEED_INCREMENTS[self.index]);
            self.input_delay = 0.00001;
        }
    }
}

pub(super) fn register_systems<'a, 'b>(
    builder: &mut DispatcherBuilder<'a, 'b>,
) -> Result<(), Error> {
    builder.add(
        SetSpeedSystem::default(),
        "speed_system",
        &["input_system", "mouse_focus"],
    );
    builder.add(
        CameraMoveSystem::default(),
        "camera_move_system",
        &["input_system", "mouse_focus"],
    );
    Ok(())
}
