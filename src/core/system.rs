use super::movement::Orbit;
use super::*;
use crate::utils;
use amethyst::{
    assets::{Handle, Prefab, PrefabData, PrefabLoaderSystemDesc, ProgressCounter},
    core::{math::Point3, Hidden, Named, Parent, ParentHierarchy, Transform},
    derive::PrefabData,
    ecs::{
        Component, DenseVecStorage, Entities, Entity, Join, Read, ReadExpect, ReadStorage, System,
        VecStorage, Write, WriteStorage,
    },
    prelude::*,
    renderer::{debug_drawing::*, palette::Srgba, SpriteRender, SpriteSheet},
};
use derive_new::new;
use rand::{thread_rng, Rng};

#[derive(Default)]
pub struct SelectedStarSystem {
    pub id: u32,
}

#[derive(Clone, Copy, Default)]
pub struct StarSystem;

impl Component for StarSystem {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy, PartialEq, Eq, PrefabData)]
#[prefab(Component)]
pub enum BodyType {
    Planet,
    Moon,
    Asteroid,
    Star,
    JumpPoint,
}

impl Component for BodyType {
    type Storage = VecStorage<Self>;
}

#[derive(new, Default, Serialize, Deserialize, Debug, Copy, Clone, PrefabData)]
#[prefab(Component)]
pub struct Atmosphere {
    pub habitability: f32,
}

impl Component for Atmosphere {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Default)]
pub struct StarSystemSwitcher {
    previous_system: u32,
    previous_options: DrawOrbitLinesOption,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct DrawOrbitLinesOption {
    pub moon: bool,
    pub planet: bool,
    pub asteroid: bool,
}

impl Default for DrawOrbitLinesOption {
    fn default() -> Self {
        DrawOrbitLinesOption {
            moon: false,
            planet: true,
            asteroid: false,
        }
    }
}

impl DrawOrbitLinesOption {
    pub fn get_bt_values(self) -> Vec<BodyType> {
        let mut r = Vec::new();
        if self.moon {
            r.push(BodyType::Moon);
        }
        if self.planet {
            r.push(BodyType::Planet);
        }
        if self.asteroid {
            r.push(BodyType::Asteroid)
        }
        r
    }
}

impl<'s> System<'s> for StarSystemSwitcher {
    type SystemData = (
        Entities<'s>,
        Read<'s, SelectedStarSystem>,
        WriteStorage<'s, Hidden>,
        WriteStorage<'s, DebugLinesComponent>,
        ReadStorage<'s, Orbit>,
        ReadStorage<'s, BodyType>,
        ReadExpect<'s, ParentHierarchy>,
        Read<'s, DrawOrbitLinesOption>,
        ReadStorage<'s, Transform>,
        ReadStorage<'s, Parent>,
    );

    fn run(
        &mut self,
        (e, ss, mut hidden, mut lines, orbit, bt, hierarchy, dol, trans, parent): Self::SystemData,
    ) {
        // Orbit Lines
        let old_system = e.entity(self.previous_system);
        let old_bodies = hierarchy.all_children(old_system);
        for (lines, _) in (&mut lines, &old_bodies).join() {
            lines.clear();
        }
        let new_system = e.entity(ss.id);
        let new_bodies = hierarchy.all_children(new_system);
        let draw_bt = dol.get_bt_values();
        for (lines, bt, orbit, parent, _) in (&mut lines, &bt, &orbit, &parent, &new_bodies).join()
        {
            lines.clear();
            if draw_bt.contains(bt) {
                let ptm = trans.get(parent.entity).unwrap().global_matrix();
                add_orbit_circle(lines, (ptm[12], ptm[13]), orbit.radius);
            }
        }
        // Sprites
        for (entity, _) in (&e, &new_bodies).join() {
            hidden.remove(entity);
        }
        for (entity, _) in (&e, &old_bodies).join() {
            hidden
                .insert(entity, Hidden)
                .expect("Failed to hide entity");
        }
        self.previous_system = ss.id;
        self.previous_options = *dol;
    }
}

pub struct DebugLineComponentAdder;

impl<'s> System<'s> for DebugLineComponentAdder {
    type SystemData = (
        Entities<'s>,
        WriteStorage<'s, DebugLinesComponent>,
        ReadStorage<'s, BodyType>,
    );

    fn run(&mut self, (e, mut lines, bt): Self::SystemData) {
        let mut t: Vec<Entity> = Vec::new();
        //Anti-storage makes lines = ();
        for (e, bt, _) in (&*e, &bt, !&lines).join() {
            debug!("Adding orbit line for {}", e.id());
            t.push(e);
        }
        for e in t.iter() {
            lines
                .insert(*e, DebugLinesComponent::default())
                .expect("Couldn't add orbit lines");
        }
    }
}

//TODO Build generic initilize star_system that takes array of planets, jump points, etc.
pub fn initialize_sol(world: &mut World, prefab: Handle<Prefab<SystemPrefrab>>) {
    let sol = world
        .create_entity()
        .named("Sol")
        .with(StarSystem)
        .with(AsteroidBelts::from(AsteroidBelt::new(1000, 100.0, 1000.0)))
        .with(prefab)
        .build();

    let mut ss = world.write_resource::<SelectedStarSystem>();
    ss.id = sol.id();
}

pub fn initialize_empty_system(world: &mut World, sprite: Handle<Prefab<SystemPrefrab>>) {
    let sol = world
        .create_entity()
        .named("Other")
        .with(StarSystem)
        .with(AsteroidBelts::from(AsteroidBelt::new(1000, 100.0, 1000.0)))
        .build();
    let sun = world
        .create_entity()
        .with(Parent::new(sol))
        .with(BodyType::Star)
        .with(Hidden)
        .build();
    world
        .create_entity()
        .with(Parent::new(sun))
        .with(BodyType::Planet)
        .with(Hidden)
        .with(Orbit::new(1.0, 100.0, 0.0))
        .build();
    initialize_lines(world);
}

fn initialize_lines(world: &mut World) {
    let e = world.entities();
    let bt = world.read_storage::<BodyType>();
    let mut l = world.write_storage::<DebugLinesComponent>();
    let mut r = Vec::new();
    for (e, bt, _) in (&e, &bt, !&l).join() {
        if bt == &BodyType::Planet || bt == &BodyType::Planet || bt == &BodyType::Moon {
            r.push(e);
        }
    }
    for e in r.iter() {
        l.insert(*e, DebugLinesComponent::default())
            .expect("Failed to add orbit lines!");
    }
    info!("Finished adding DebugLinesComponent");
}

fn create_bodies(
    world: &mut World,
    parent: Entity,
    sprite: SpriteRender,
    count: u32,
) -> EntityWrapper {
    let mut r = Vec::new();
    for _ in 0..count {
        r.push(
            world
                .create_entity()
                .with(Parent::new(parent))
                .with(Transform::default())
                .with(sprite.clone())
                .build(),
        );
    }
    EntityWrapper(r)
}

struct EntityWrapper(Vec<Entity>);

impl EntityWrapper {
    fn hide(self, world: &mut World) -> Self {
        let mut h = world.write_storage::<Hidden>();
        self.0.iter().for_each(|e| {
            h.insert(*e, Hidden).expect("Failed to hide entity");
        });
        self
    }
    fn with_body_type(self, world: &mut World, bt: BodyType) -> Self {
        let mut h = world.write_storage::<BodyType>();
        self.0.iter().for_each(|e| {
            h.insert(*e, bt).expect("Failed to set body type");
        });
        self
    }
    fn with_orbit_between(self, world: &mut World, min: f32, max: f32) -> Self {
        let mut h = world.write_storage::<Orbit>();
        let mut rng = thread_rng();
        self.0.iter().for_each(|e| {
            let o = Orbit::new(
                rng.gen_range(1.0, 10.0),
                rng.gen_range(min, max),
                rng.gen_range(-3.10, 3.10),
            );
            h.insert(*e, o).expect("Failed to set body type");
        });
        self
    }
}

fn add_orbit_circle(dbc: &mut DebugLinesComponent, center: (f32, f32), radius: f32) {
    dbc.add_circle_2d(
        Point3::new(center.0, center.1, 0.0),
        radius,
        100,
        utils::SRGBA_GREEN,
    );
}

pub fn draw_temp_sphere(dl: &mut DebugLines, x: f32, y: f32, radius: f32, color: Option<Srgba>) {
    dl.draw_sphere(
        Point3::new(x, y, 0.0),
        radius,
        100,
        100,
        color.unwrap_or_else(|| utils::SRGBA_GREEN),
    );
}

#[derive(new, Serialize, Deserialize, Debug, Clone, Copy)]
pub struct AsteroidBelt {
    count: u32,
    range_min: f32,
    range_max: f32,
}

#[derive(new, Serialize, Deserialize, Debug, Clone, Component, PrefabData)]
#[prefab(Component)]
pub struct AsteroidBelts(Vec<AsteroidBelt>);

impl From<AsteroidBelt> for AsteroidBelts {
    fn from(a: AsteroidBelt) -> Self {
        AsteroidBelts::new(vec![a])
    }
}

#[derive(Debug, Deserialize, Serialize, PrefabData)]
pub enum SystemPrefrab {
    StarSystemPrefab {
        name: Named,
        belts: AsteroidBelts,
    },
    StarPrefab {
        name: Named,
        #[serde(default)]
        transform: Transform,
        body_type: BodyType,
    },
    OrbitalBodyPrefab {
        name: Named,
        #[serde(default)]
        transform: Transform,
        body_type: BodyType,
        orbit: Orbit,
        atmosphere: Option<Atmosphere>,
    },
}

pub(super) fn register_systems<'a, 'b>(
    world: &mut World,
    builder: &mut DispatcherBuilder<'a, 'b>,
) -> Result<(), Error> {
    builder.add(ColonyPopulationUpdater, "colony_population", &[]);
    builder.add(
        PrefabLoaderSystemDesc::<SystemPrefrab>::default().build(world),
        "prefab_loader_system",
        &[],
    );
    builder.add(DebugLineComponentAdder, "dlc_adder", &[]);
    builder.add(
        StarSystemSwitcher::default(),
        "system_switcher",
        &["dlc_adder", "imgui"],
    );
    Ok(())
}
