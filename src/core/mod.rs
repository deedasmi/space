#![allow(unused_variables)]
#![allow(unused_imports)]
pub mod controls;
pub mod faction;
pub mod gui;
pub mod movement;
pub mod system;
pub mod time;

pub use controls::*;
pub use faction::*;
pub use gui::ImguiSystem;
pub use movement::*;
pub use system::*;
pub use time::*;

use amethyst::{
    assets::{PrefabData, ProgressCounter},
    core::{SystemBundle, Time, Transform},
    derive::PrefabData,
    ecs::{
        prelude::{DispatcherBuilder, World},
        Component, Entity, Join, Read, ReadStorage, System, VecStorage, Write, WriteStorage,
    },
    Error,
};
use log::*;
use serde::{Deserialize, Serialize};

pub struct CoreBundle;

impl<'a, 'b> SystemBundle<'a, 'b> for CoreBundle {
    fn build(
        self,
        world: &mut World,
        builder: &mut DispatcherBuilder<'a, 'b>,
    ) -> Result<(), Error> {
        movement::register_systems(builder)?;
        time::register_systems(builder)?;
        gui::register_systems(builder)?;
        system::register_systems(world, builder)?;
        controls::register_systems(builder)?;
        Ok(())
    }
}
