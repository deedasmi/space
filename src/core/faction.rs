use super::*;
use crate::utils::YEAR;
use amethyst::{
    core::Time,
    ecs::{
        Component, DenseVecStorage, Entity, Join, ParJoin, Read, ReadStorage, System, SystemData,
        WriteStorage,
    },
};
use derive_new::new;

pub struct Faction;

impl Component for Faction {
    type Storage = DenseVecStorage<Self>;
}

///Stores information about a colony
///Population value in millions
#[derive(Debug, new)]
pub struct Colony {
    faction: Entity,
    pub planet: Entity,
    population: f32,
    #[new(value = "100.0")]
    loyalty: f32,
}

impl Component for Colony {
    type Storage = DenseVecStorage<Self>;
}

impl Colony {
    pub fn update(&mut self, time: f32, habit: f32) {
        // TODO infrastucture reduces casulties?
        self.population +=
            self.population * (self.loyalty / 10.0 * habit - 8.0) / 100.0 * (time / YEAR);
    }
}

pub struct ColonyPopulationUpdater;

impl<'s> System<'s> for ColonyPopulationUpdater {
    type SystemData = (
        WriteStorage<'s, Colony>,
        ReadStorage<'s, Atmosphere>,
        Read<'s, Time>,
    );

    fn run(&mut self, (mut c, a, t): Self::SystemData) {
        for colony in (&mut c).join() {
            colony.update(
                t.delta_seconds(),
                a.get(colony.planet).map(|x| x.habitability).unwrap_or(0.01),
            );
        }
    }
}
