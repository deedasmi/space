use super::{DrawOrbitLinesOption, GameTime, SelectedStarSystem, StarSystem};
use amethyst::{
    core::{math::Point3, Named, Time, Transform},
    derive::SystemDesc,
    ecs::{
        DispatcherBuilder, Entities, Join, Read, ReadExpect, ReadStorage, System, SystemData, Write,
    },
    renderer::Camera,
    utils::fps_counter::FpsCounter,
    window::ScreenDimensions,
    Error,
};
use amethyst_imgui::imgui::*;

#[derive(SystemDesc, Default)]
pub struct ImguiSystem {
    dropdown_selection: usize,
}

impl<'s> System<'s> for ImguiSystem {
    type SystemData = (
        Entities<'s>,
        Read<'s, GameTime>,
        Read<'s, Time>,
        ReadExpect<'s, ScreenDimensions>,
        ReadStorage<'s, Camera>,
        ReadStorage<'s, Transform>,
        Write<'s, SelectedStarSystem>,
        ReadStorage<'s, StarSystem>,
        ReadStorage<'s, Named>,
        Read<'s, FpsCounter>,
        Write<'s, DrawOrbitLinesOption>,
    );

    fn run(
        &mut self,
        (e, gt, gs, sd, camera, trans, mut selected_ss, ss, name, fps, mut dol): Self::SystemData,
    ) {
        use std::borrow::Cow;
        //TODO Cache this
        let mut ssnames: Vec<Cow<'static, str>> = Vec::new();
        let mut ssids: Vec<u32> = Vec::new();
        for (e, name, _) in (&e, &name, &ss).join() {
            ssnames.push(name.name.to_owned());
            ssids.push(e.id());
        }
        // Don't build GUI until systems have been created
        if ssnames.is_empty() {
            return;
        }
        let ssnames: Vec<_> = ssnames
            .iter()
            .map(|x| Cow::from(im_str!("{}", x)))
            .collect();
        amethyst_imgui::with(|ui| {
            Window::new(im_str!("Time"))
                .size([400.0, 450.0], Condition::FirstUseEver)
                .build(&ui, || {
                    ui.text(im_str!("Time: {}", gt.format()));
                    ui.text(im_str!("Game Speed: {}", gs.time_scale()));
                    ui.text(im_str!("Game Time Per Tick: {}", gs.delta_seconds()));
                    ui.text(im_str!("FPS: {}", fps.frame_fps()));
                    let mouse_pos = ui.io().mouse_pos;
                    ui.text(format!(
                        "Mouse Window: ({:.1}, {:.1})",
                        mouse_pos[0], mouse_pos[1]
                    ));
                    let pos = Point3::<f32>::new(mouse_pos[0], mouse_pos[1], 0.0);
                    for (c, t) in (&camera, &trans).join() {
                        let res = c.projection().screen_to_world_point(pos, sd.diagonal(), t);
                        ui.text(format!("Mouse World: ({:.1}, {:.1})", res[0], res[1]));
                    }
                    ui.text(im_str!(
                        "Selected System: {}",
                        ssnames[ssids.iter().position(|x| x == &selected_ss.id).unwrap()]
                    ));

                    // TODO only discovered systems
                    ComboBox::new(im_str!("System Selector")).build_simple(
                        ui,
                        &mut self.dropdown_selection,
                        &ssnames[..],
                        &|x| x.clone(),
                    );
                    ui.checkbox(im_str!("Draw Planet Orbits: "), &mut dol.planet);
                    ui.checkbox(im_str!("Draw Moon Orbits: "), &mut dol.moon);
                    ui.checkbox(im_str!("Draw Asteroid Orbits: "), &mut dol.asteroid);
                });
        });
        selected_ss.id = ssids[self.dropdown_selection];
    }
}

pub(super) fn register_systems<'a, 'b>(
    builder: &mut DispatcherBuilder<'a, 'b>,
) -> Result<(), Error> {
    builder.add(ImguiSystem::default(), "imgui", &["time_system"]);
    Ok(())
}
