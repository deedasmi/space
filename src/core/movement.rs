use super::*;
use amethyst::renderer::debug_drawing::*;
use derive_new::new;

pub type OrbitData<'a> = (
    WriteStorage<'a, movement::Orbit>,
    WriteStorage<'a, Transform>,
    Read<'a, Time>,
    ReadStorage<'a, BodyType>,
);

#[derive(new, Clone, Serialize, Debug, Deserialize, PrefabData)]
#[prefab(Component)]
pub struct Orbit {
    kps: f32,
    pub radius: f32,
    #[serde(default)]
    radian: f32,
}

impl Component for Orbit {
    type Storage = VecStorage<Self>;
}

impl Orbit {
    pub fn next(&mut self, delta: f32) {
        self.radian += ((self.kps * delta) % self.radius) / self.radius;
    }
    pub fn get_x(&self) -> f32 {
        self.radian.cos() * self.radius
    }
    pub fn get_y(&self) -> f32 {
        self.radian.sin() * self.radius
    }
}

pub struct OrbitSystem;

impl<'a> System<'a> for OrbitSystem {
    type SystemData = OrbitData<'a>;

    fn run(&mut self, (mut orbit, mut transforms, time, bt): Self::SystemData) {
        for (orbit, transform, bt) in (&mut orbit, &mut transforms, &bt).join() {
            //Skip asteroids in systems that aren't focused
            orbit.next(time.delta_seconds());
            transform.set_translation_x(orbit.get_x());
            transform.set_translation_y(orbit.get_y());
        }
    }
}

pub(super) fn register_systems<'a, 'b>(
    builder: &mut DispatcherBuilder<'a, 'b>,
) -> Result<(), Error> {
    builder.add(OrbitSystem, "orbit_system", &[]);
    Ok(())
}
