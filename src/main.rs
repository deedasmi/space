#![allow(dead_code)]
pub mod core;
pub mod utils;

use crate::core::*;
use amethyst::{
    assets::{AssetStorage, Handle, Loader, Prefab, PrefabLoader, ProgressCounter, RonFormat},
    controls::MouseFocusUpdateSystemDesc,
    core::transform::TransformBundle,
    input::{InputBundle, StringBindings},
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle, *,
    },
    utils::{application_root_dir, fps_counter::FpsCounterBundle},
};
use amethyst_imgui::RenderImgui;
use derive_new::new;
use log::*;
use utils::*;

pub const SYSTEM_HEIGHT: f32 = 1000.0;
pub const SYSTEM_WIDTH: f32 = 1000.0;

#[derive(new)]
struct InitState {
    pfs: Vec<Handle<Prefab<SystemPrefrab>>>,
}

#[derive(new)]
struct LoadingState {
    #[new(default)]
    pc: ProgressCounter,
    #[new(default)]
    pfs: Vec<Handle<Prefab<SystemPrefrab>>>,
}

impl SimpleState for LoadingState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        info!("Loading Assets");
        let world = data.world;
        world.register::<StarSystem>();
        world.register::<AsteroidBelts>();
        //let sprite = load_sprite_sheet(world);
        self.pfs.push(load_sol_prefab(world, &mut self.pc));

        initialize_camera(world);
    }

    fn update(&mut self, _: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        if self.pc.num_failed() > 0 {
            //error!("Failed to Load!: {:?}", self.pc.errors());
            return Trans::Quit;
        } else if self.pc.is_complete() {
            info!("Finished loading Assets");
            return Trans::Switch(Box::new(InitState::new(self.pfs.clone())));
        }
        Trans::None
    }
}

impl SimpleState for InitState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        info!("Init State Started");
        let world = data.world;
        //let sprite = load_sprite_sheet(world);

        initialize_camera(world);
        //world.create_entity().named("Player Empire").build();
        core::initialize_sol(world, self.pfs[0].clone());
        core::initialize_empty_system(world, self.pfs[0].clone());
        //let mut t = world.write_resource::<Time>();
        //t.set_time_scale(100000.0);
    }

    /*fn update(&mut self) -> SimpleTrans {
        println!("Hello from Amethyst!");
        Trans::Quit
    }

    fn on_stop(&mut self) {
        println!("Game stopped!");
    }
    */
}

fn load_sprite_sheet(world: &mut World) -> Handle<SpriteSheet> {
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "assets/sprite.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };
    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "assets/sprite.ron",
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}

fn load_sol_prefab(world: &mut World, pc: &mut ProgressCounter) -> Handle<Prefab<SystemPrefrab>> {
    info!("Loading Sol");
    //let loader = world.read_resource::<PrefabLoader<SystemPrefrab>>();
    world.exec(|loader: PrefabLoader<'_, SystemPrefrab>| {
        loader.load("prefabs/systems/sol.ron", RonFormat, pc)
    })
}

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());
    let app_root = application_root_dir()?;
    let resources = app_root.join("resources");
    let display_config_path = resources.join("display.ron");
    let input_config = resources.join("bindings.ron");
    let input_bundle =
        InputBundle::<StringBindings>::new().with_bindings_from_file(input_config)?;

    let game_data = GameDataBuilder::default()
        .with_bundle(input_bundle)?
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(display_config_path)?
                        .with_clear([0.0, 0.0, 0.0, 1.0]),
                )
                .with_plugin(RenderFlat2D::default())
                .with_plugin(RenderImgui::<amethyst::input::StringBindings>::default())
                .with_plugin(RenderDebugLines::default()),
        )?
        .with_bundle(FpsCounterBundle::default())?
        .with_system_desc(MouseFocusUpdateSystemDesc::default(), "mouse_focus", &[])
        .with_bundle(CoreBundle)?
        .with_bundle(TransformBundle::new().with_dep(&["orbit_system"]))?;

    Application::build(resources, LoadingState::new())?
        .build(game_data)?
        .run();
    Ok(())
}
