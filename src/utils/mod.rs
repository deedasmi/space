use amethyst::{
    core::Transform,
    prelude::*,
    renderer::{
        palette::{rgb::Rgb, Srgb, Srgba},
        *,
    },
};
use std::marker::PhantomData;

pub const YEAR: f32 = 31_556_952.0;

//TODO probably should move these to seperate module.
const RGB_GREEN: Srgb<f32> = Rgb {
    red: 0.0,
    green: 1.0,
    blue: 0.0,
    standard: PhantomData,
};
pub const SRGBA_GREEN: Srgba = Srgba {
    color: RGB_GREEN,
    alpha: 1.0,
};
const RGB_YELLOW: Srgb<f32> = Rgb {
    red: 1.0,
    green: 1.0,
    blue: 0.0,
    standard: PhantomData,
};
pub const SRGBA_YELLOW: Srgba = Srgba {
    color: RGB_YELLOW,
    alpha: 1.0,
};

pub fn initialize_camera(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_translation_xyz(0.0, 0.0, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(1000.0, 1000.0))
        .with(transform)
        .build();
}
